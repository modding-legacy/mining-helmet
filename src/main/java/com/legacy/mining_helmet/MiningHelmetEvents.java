package com.legacy.mining_helmet;

import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.Mob;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;

public class MiningHelmetEvents
{
	/*@SubscribeEvent*/
	public static void onMobSpawn(Mob entity)
	{
		if (entity.getType().is(MiningHelmetTags.MINING_MOBS) && MiningHelmetConfig.minerSpawnChance() > 0)
		{
			if (entity.getY() < entity.level.getSeaLevel() && entity.getRandom().nextInt(MiningHelmetConfig.minerSpawnChance()) == 0)
			{
				entity.setItemSlot(EquipmentSlot.HEAD, new ItemStack(MiningHelmetRegistry.MINING_HELMET.get()));
				entity.setItemSlot(EquipmentSlot.MAINHAND, new ItemStack(Items.IRON_PICKAXE));
			}
		}
	}
}
