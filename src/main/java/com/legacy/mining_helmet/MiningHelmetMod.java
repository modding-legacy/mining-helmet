package com.legacy.mining_helmet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.legacy.mining_helmet.client.MiningHelmetClient;
import com.legacy.mining_helmet.client.ModModelLayers;

import net.minecraft.resources.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.ModList;
import net.minecraftforge.fml.ModLoadingContext;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.config.ModConfig;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;

@Mod(MiningHelmetMod.MODID)
public class MiningHelmetMod
{
	public static final Logger LOGGER = LogManager.getLogger();
	public static final String MODID = "mining_helmet";

	public MiningHelmetMod()
	{
		ModLoadingContext.get().registerConfig(ModConfig.Type.COMMON, MiningHelmetConfig.SERVER_SPEC);

		var modBus = FMLJavaModLoadingContext.get().getModEventBus();

		DistExecutor.unsafeRunWhenOn(Dist.CLIENT, () -> () ->
		{
			modBus.addListener(ModModelLayers::initPost);
			ModModelLayers.init();
			MiningHelmetClient.init();
		});


		/*MinecraftForge.EVENT_BUS.register(MiningHelmetEvents.class);*/
		modBus.register(MiningHelmetRegistry.class);
		modBus.addListener(MiningHelmetRegistry::registerCreativeTabs);
	}

	public static ResourceLocation locate(String name)
	{
		return new ResourceLocation(MiningHelmetMod.MODID, name);
	}

	public static boolean lucentPresent()
	{
		return ModList.get().isLoaded("lucent");
	}
}
