package com.legacy.mining_helmet.item;

import java.util.EnumMap;
import java.util.function.Supplier;

import com.legacy.mining_helmet.MiningHelmetMod;

import net.minecraft.Util;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.world.item.ArmorItem;
import net.minecraft.world.item.ArmorMaterial;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.common.util.Lazy;

public enum HelmetArmorMaterials implements ArmorMaterial
{
	MINING("mining_helmet", 15, Util.make(new EnumMap<>(ArmorItem.Type.class), (p_266649_) ->
	{
		p_266649_.put(ArmorItem.Type.BOOTS, 3);
		p_266649_.put(ArmorItem.Type.LEGGINGS, 6);
		p_266649_.put(ArmorItem.Type.CHESTPLATE, 8);
		p_266649_.put(ArmorItem.Type.HELMET, 3);
	}), 9, SoundEvents.ARMOR_EQUIP_IRON, 0.0F, 0.0F, () -> Ingredient.of(Items.IRON_INGOT));

	private static final EnumMap<ArmorItem.Type, Integer> HEALTH_FUNCTION_FOR_TYPE = Util.make(new EnumMap<>(ArmorItem.Type.class), (p_266653_) ->
	{
		p_266653_.put(ArmorItem.Type.BOOTS, 13);
		p_266653_.put(ArmorItem.Type.LEGGINGS, 15);
		p_266653_.put(ArmorItem.Type.CHESTPLATE, 16);
		p_266653_.put(ArmorItem.Type.HELMET, 11);
	});

	private final String name;
	private final int maxDamageFactor;
	private final EnumMap<ArmorItem.Type, Integer> damageReductionAmountArray;
	private final int enchantability;
	private final SoundEvent soundEvent;
	private final float toughness;
	private final float knockbackResistance;
	private final Lazy<Ingredient> repairMaterial;

	private HelmetArmorMaterials(String nameIn, int maxDamageFactorIn, EnumMap<ArmorItem.Type, Integer> damageReductionAmountsIn, int enchantabilityIn, SoundEvent equipSoundIn, float p_i48533_8_, float knockbackResistIn, Supplier<Ingredient> repairMaterialSupplier)
	{
		this.name = MiningHelmetMod.locate(nameIn).toString();
		this.maxDamageFactor = maxDamageFactorIn;
		this.damageReductionAmountArray = damageReductionAmountsIn;
		this.enchantability = enchantabilityIn;
		this.soundEvent = equipSoundIn;
		this.toughness = p_i48533_8_;
		this.knockbackResistance = knockbackResistIn;
		this.repairMaterial = Lazy.of(repairMaterialSupplier);
	}

	@Override
	public int getDurabilityForType(ArmorItem.Type p_266745_)
	{
		return HEALTH_FUNCTION_FOR_TYPE.get(p_266745_) * this.maxDamageFactor;
	}

	@Override
	public int getDefenseForType(ArmorItem.Type p_266752_)
	{
		return this.damageReductionAmountArray.get(p_266752_);
	}

	@Override
	public int getEnchantmentValue()
	{
		return this.enchantability;
	}

	@Override
	public SoundEvent getEquipSound()
	{
		return this.soundEvent;
	}

	@Override
	public Ingredient getRepairIngredient()
	{
		return this.repairMaterial.get();
	}

	@OnlyIn(Dist.CLIENT)
	@Override
	public String getName()
	{
		return this.name;
	}

	@Override
	public float getToughness()
	{
		return this.toughness;
	}

	@Override
	public float getKnockbackResistance()
	{
		return this.knockbackResistance;
	}
}