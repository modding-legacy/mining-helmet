package com.legacy.mining_helmet.item;

import java.util.function.Consumer;

import com.legacy.mining_helmet.MiningHelmetConfig;
import com.legacy.mining_helmet.client.ModModelLayers;
import com.legacy.mining_helmet.client.model.MiningHelmetModel;

import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.item.ArmorItem;
import net.minecraft.world.item.ArmorMaterial;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.client.extensions.common.IClientItemExtensions;
import net.minecraftforge.common.util.NonNullLazy;

public class MiningHelmetItem extends ArmorItem
{
	public MiningHelmetItem(ArmorMaterial materialIn, ArmorItem.Type slot, Properties builder)
	{
		super(materialIn, slot, builder);
	}

	@OnlyIn(Dist.CLIENT)
	@Override
	public void initializeClient(Consumer<IClientItemExtensions> consumer)
	{
		consumer.accept(Rendering.INSTANCE);
	}

	@OnlyIn(Dist.CLIENT)
	private static final class Rendering implements IClientItemExtensions
	{
		private static final Rendering INSTANCE = new MiningHelmetItem.Rendering();

		private final NonNullLazy<MiningHelmetModel<LivingEntity>> helmet = NonNullLazy.of(() -> new MiningHelmetModel<LivingEntity>(getModel().bakeLayer(ModModelLayers.MINING_HELMET)));
		private final NonNullLazy<MiningHelmetModel<LivingEntity>> helmet_lifted = NonNullLazy.of(() -> new MiningHelmetModel<LivingEntity>(getModel().bakeLayer(ModModelLayers.MINING_HELMET_LIFTED)));

		private Rendering()
		{
		}

		@Override
		public net.minecraft.client.model.HumanoidModel<?> getHumanoidArmorModel(LivingEntity wearer, ItemStack stack, EquipmentSlot slot, net.minecraft.client.model.HumanoidModel<?> defaultModel)
		{
			return !MiningHelmetConfig.helmetCoversFace() ? this.helmet_lifted.get() : this.helmet.get();
		}

		@OnlyIn(Dist.CLIENT)
		private static net.minecraft.client.model.geom.EntityModelSet getModel()
		{
			return net.minecraft.client.Minecraft.getInstance().getEntityModels();
		}
	}
}
