package com.legacy.mining_helmet;

import net.minecraft.tags.TagKey;
import net.minecraft.world.entity.EntityType;
import net.minecraftforge.registries.ForgeRegistries;

public class MiningHelmetTags
{
	public static final TagKey<EntityType<?>> MINING_MOBS = TagKey.create(ForgeRegistries.Keys.ENTITY_TYPES, MiningHelmetMod.locate("miner_mobs"));

}
