package com.legacy.mining_helmet.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import com.legacy.mining_helmet.MiningHelmetConfig;
import com.legacy.mining_helmet.client.DynamicLightingManager;

import net.minecraft.core.BlockPos;
import net.minecraft.world.level.lighting.BlockLightEngine;

@Mixin(BlockLightEngine.class)
public class BlockLightEngineMixin
{
	@Inject(at = @At(value = "RETURN"), method = "getLightEmission(J)I", cancellable = true)
	private void modifyLightValue(long longPos, CallbackInfoReturnable<Integer> callback)
	{
		BlockPos pos = BlockPos.of(longPos);
		if (DynamicLightingManager.SOURCES.containsKey(pos) && DynamicLightingManager.SOURCES.get(pos).shouldStay)
			callback.setReturnValue(MiningHelmetConfig.getHelmetLightLevel());
	}
}
