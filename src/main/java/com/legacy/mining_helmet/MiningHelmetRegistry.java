package com.legacy.mining_helmet;

import com.legacy.mining_helmet.item.HelmetArmorMaterials;
import com.legacy.mining_helmet.item.MiningHelmetItem;

import net.minecraft.world.item.ArmorItem;
import net.minecraft.world.item.CreativeModeTab.TabVisibility;
import net.minecraft.world.item.CreativeModeTabs;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.Items;
import net.minecraftforge.common.util.Lazy;
import net.minecraftforge.event.CreativeModeTabEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegisterEvent;

public class MiningHelmetRegistry
{
	public static final Lazy<Item> MINING_HELMET = Lazy.of(() -> new MiningHelmetItem(HelmetArmorMaterials.MINING, ArmorItem.Type.HELMET, new Item.Properties()));

	@SubscribeEvent
	public static void onRegister(RegisterEvent event)
	{
		if (event.getRegistryKey().equals(ForgeRegistries.Keys.ITEMS))
			event.register(ForgeRegistries.Keys.ITEMS, MiningHelmetMod.locate("mining_helmet"), MINING_HELMET);
	}

	public static void registerCreativeTabs(CreativeModeTabEvent.BuildContents event)
	{
		if (event.getTab() == CreativeModeTabs.COMBAT)
		{
			for (var e : event.getEntries())
			{
				if (e.getKey().getItem() == Items.TURTLE_HELMET)
				{
					event.getEntries().putAfter(e.getKey(), MINING_HELMET.get().getDefaultInstance(), TabVisibility.PARENT_AND_SEARCH_TABS);
					break;
				}
			}
		}
	}
}