package com.legacy.mining_helmet.client;

import com.legacy.mining_helmet.MiningHelmetConfig;

import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.ModLoadingContext;
import net.minecraftforge.fml.config.ModConfig;

@OnlyIn(Dist.CLIENT)
public class MiningHelmetClient
{
	public static void init()
	{
		ModLoadingContext.get().registerConfig(ModConfig.Type.CLIENT, MiningHelmetConfig.CLIENT_SPEC);
		MinecraftForge.EVENT_BUS.register(MiningHelmetClientEvents.class);
	}
}