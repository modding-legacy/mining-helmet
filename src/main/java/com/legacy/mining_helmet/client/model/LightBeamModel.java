package com.legacy.mining_helmet.client.model;

import java.util.List;

import com.legacy.mining_helmet.MiningHelmetMod;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;

import net.minecraft.client.Minecraft;
import net.minecraft.client.model.HumanoidModel;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.CubeDeformation;
import net.minecraft.client.model.geom.builders.CubeListBuilder;
import net.minecraft.client.model.geom.builders.LayerDefinition;
import net.minecraft.client.model.geom.builders.MeshDefinition;
import net.minecraft.client.model.geom.builders.PartDefinition;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.LivingEntity;

public class LightBeamModel<T extends LivingEntity> extends HumanoidModel<T>
{
	private static final ResourceLocation TEXTURE_LIGHT = MiningHelmetMod.locate("textures/models/armor/mining_helmet_light.png");

	public ModelPart lightBeam;

	public LightBeamModel(ModelPart model)
	{
		super(model);
		this.lightBeam = model.getChild("light_beam");
	}

	public static LayerDefinition createBeamLayer(CubeDeformation size, boolean coverFace)
	{
		MeshDefinition mesh = HumanoidModel.createMesh(size, 0.0F);
		PartDefinition root = mesh.getRoot();

		CubeDeformation scale = CubeDeformation.NONE;

		float offsetAmount = !coverFace ? 2 : 1;
		root.addOrReplaceChild("light_beam", CubeListBuilder.create().texOffs(0, 0).addBox(-2.0F, -9.0F - offsetAmount, -13.0F, 4, 4, 8, scale), PartPose.ZERO);

		return LayerDefinition.create(mesh, 24, 12);
	}

	@Override
	protected Iterable<ModelPart> headParts()
	{
		float offset = this.head.y;

		this.lightBeam.copyFrom(this.head);

		this.lightBeam.y = offset;

		return List.of(this.lightBeam);
	}

	@Override
	protected Iterable<ModelPart> bodyParts()
	{
		return List.of();
	}

	@Override
	public void setupAnim(T entityIn, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch)
	{
		super.setupAnim(entityIn, limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch);
	}

	@Override
	public void renderToBuffer(PoseStack matrixStackIn, VertexConsumer bufferIn, int packedLightIn, int packedOverlayIn, float red, float green, float blue, float alpha)
	{
		VertexConsumer ivertexbuilder = Minecraft.getInstance().renderBuffers().bufferSource().getBuffer(RenderType.entityTranslucent(TEXTURE_LIGHT));

		this.headParts().forEach((modelRenderer) ->
		{
			float intensef = 1.0F + ((packedLightIn / 8)) * 0.01F;
			System.out.println(intensef);

			modelRenderer.render(matrixStackIn, ivertexbuilder, packedLightIn, packedOverlayIn, red, green, blue, alpha * (intensef));
		});
	}
}
