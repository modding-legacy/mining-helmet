package com.legacy.mining_helmet.client.model;

import java.util.List;

import net.minecraft.client.model.HumanoidModel;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.CubeDeformation;
import net.minecraft.client.model.geom.builders.CubeListBuilder;
import net.minecraft.client.model.geom.builders.LayerDefinition;
import net.minecraft.client.model.geom.builders.MeshDefinition;
import net.minecraft.client.model.geom.builders.PartDefinition;
import net.minecraft.world.entity.LivingEntity;

public class MiningHelmetModel<T extends LivingEntity> extends HumanoidModel<T>
{
	/*private static final ResourceLocation TEXTURE = MiningHelmetMod.locate("textures/models/armor/mining_helmet.png");
	private static final ResourceLocation TEXTURE_LIGHT = MiningHelmetMod.locate("textures/models/armor/mining_helmet_light.png");
	
	private final NonNullLazy<LightBeamModel<LivingEntity>> beam = NonNullLazy.of(() -> new LightBeamModel<LivingEntity>(net.minecraft.client.Minecraft.getInstance().getEntityModels().bakeLayer(ModModelLayers.LIGHT_BEAM)));*/

	public ModelPart base, lid, light;

	public MiningHelmetModel(ModelPart model)
	{
		super(model);

		this.lid = model.getChild("lid");
		this.base = model.getChild("base");
		this.light = model.getChild("light");
	}

	public static LayerDefinition createHelmetLayer(CubeDeformation size, boolean coverFace)
	{
		MeshDefinition mesh = HumanoidModel.createMesh(size, 0.0F);
		PartDefinition root = mesh.getRoot();

		CubeDeformation scale = CubeDeformation.NONE;

		float offsetAmount = !coverFace ? 2 : 1;
		root.addOrReplaceChild("lid", CubeListBuilder.create().texOffs(0, 16).addBox(-6.0F, -4.0F - offsetAmount, -6.0F, 12, 2, 12, scale), PartPose.ZERO);
		root.addOrReplaceChild("base", CubeListBuilder.create().texOffs(0, 0).addBox(-4.0F, -8.0F - offsetAmount, -4.0F, 8, 8, 8, new CubeDeformation(1.0F)), PartPose.ZERO);
		root.addOrReplaceChild("light", CubeListBuilder.create().texOffs(32, 0).addBox(-3.0F, -10.0F - offsetAmount, -7.0F, 6, 6, 3, scale), PartPose.ZERO);

		return LayerDefinition.create(mesh, 64, 64);
	}

	@Override
	protected Iterable<ModelPart> headParts()
	{
		float offset = this.head.y;

		this.lid.copyFrom(this.head);
		this.base.copyFrom(this.head);
		this.light.copyFrom(this.head);

		this.lid.y = offset;
		this.base.y = offset;
		this.light.y = offset;

		return List.of(this.base, this.lid, light);
	}

	@Override
	protected Iterable<ModelPart> bodyParts()
	{
		return List.of();
	}

	/*@Override
	public void setupAnim(T entityIn, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch)
	{
		super.setupAnim(entityIn, limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch);
		this.beam.get().setupAnim(entityIn, limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch);
	}*/

	/*@SuppressWarnings("unchecked")
	@Override
	public void renderToBuffer(PoseStack matrixStackIn, VertexConsumer bufferIn, int packedLightIn, int packedOverlayIn, float red, float green, float blue, float alpha)
	{
		super.renderToBuffer(matrixStackIn, bufferIn, packedLightIn, packedOverlayIn, red, green, blue, alpha);
	
		this.copyPropertiesTo((HumanoidModel<T>) beam.get());
		beam.get().renderToBuffer(matrixStackIn, bufferIn, packedLightIn, packedOverlayIn, red, green, blue, alpha);
	}*/
}
