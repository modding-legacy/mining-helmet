package com.legacy.mining_helmet.client;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.legacy.mining_helmet.MiningHelmetConfig;
import com.legacy.mining_helmet.MiningHelmetMod;
import com.legacy.mining_helmet.MiningHelmetRegistry;

import net.minecraft.client.Minecraft;
import net.minecraft.core.BlockPos;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.level.ClipContext;
import net.minecraft.world.phys.HitResult;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

/**
 * Before you try to use this code for some reason, if you're looking to
 * implement dynamic lighting in your mod, instead we've made an API, Lucent.
 * The API is much better looking, compatible, and more performant than this
 * code.
 * 
 * ---- https://gitlab.com/modding-legacy/lucent ----
 * 
 * We highly recommend the use of our API made for this purpose, rather than
 * selfishly ignoring our license terms, and copy-pasting this old code. (You
 * would also be making yourself incompatible with this mod in the event you
 * did.)
 * 
 * @author David
 *
 */
@OnlyIn(Dist.CLIENT)
public class DynamicLightingManager
{
	private static final Minecraft MC = Minecraft.getInstance();
	public static final Map<BlockPos, LightData> SOURCES = new ConcurrentHashMap<>();

	public static void tick()
	{
		if (MiningHelmetMod.lucentPresent())
			return;

		if (MC.player != null && MC.level != null && MC.player.tickCount % MiningHelmetConfig.lightRefreshRate() == 0)
		{
			SOURCES.forEach((blockPos, data) -> data.shouldStay = false);

			MC.level.getEntitiesOfClass(LivingEntity.class, MC.player.getBoundingBox().inflate(MiningHelmetConfig.maxVisibleDistance()), DynamicLightingManager::shouldGlow).forEach(e -> SOURCES.put(e.blockPosition().above((int) e.getEyeHeight()), new LightData()));

			if (!SOURCES.isEmpty())
			{
				SOURCES.forEach((blockPos, data) -> MC.level.getChunkSource().getLightEngine().checkBlock(blockPos));
				SOURCES.entrySet().removeIf(entry -> !entry.getValue().shouldStay);
			}
		}
	}

	public static boolean shouldGlow(LivingEntity entity)
	{
		if (MiningHelmetMod.lucentPresent())
			return false;

		if (entity.getItemBySlot(EquipmentSlot.HEAD).getItem() == MiningHelmetRegistry.MINING_HELMET.get())
		{
			boolean visible = MiningHelmetConfig.seeThroughWalls();
			if (!visible)
			{
				visible = MC.player.level.clip(new ClipContext(MC.player.position(), entity.getEyePosition(), ClipContext.Block.VISUAL, ClipContext.Fluid.NONE, entity)).getType() == HitResult.Type.MISS;

				if (!visible && MC.player.distanceTo(entity) < 24)
					visible = true;
			}
			return visible;
		}
		return false;
	}

	public static void cleanUp()
	{
		if (MiningHelmetMod.lucentPresent())
			return;

		if (SOURCES.size() > 0 && MC.level != null)
		{
			MiningHelmetMod.LOGGER.info(String.format("Cleaning up light data for %s light sources", SOURCES.size()));
			SOURCES.forEach((blockPos, data) ->
			{
				data.shouldStay = false;
				MC.level.getChunkSource().getLightEngine().checkBlock(blockPos);
			});
			SOURCES.clear();
		}
	}

	public static class LightData
	{
		public boolean shouldStay = true;
	}
}