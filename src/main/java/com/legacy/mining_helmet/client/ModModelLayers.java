package com.legacy.mining_helmet.client;

import com.legacy.mining_helmet.MiningHelmetMod;
import com.legacy.mining_helmet.client.model.MiningHelmetModel;

import net.minecraft.client.model.geom.ModelLayerLocation;
import net.minecraft.client.model.geom.builders.CubeDeformation;
import net.minecraftforge.client.event.EntityRenderersEvent;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;

public class ModModelLayers
{
	public static final ModelLayerLocation MINING_HELMET = new ModelLayerLocation(MiningHelmetMod.locate("mining_helmet"), "main");
	public static final ModelLayerLocation MINING_HELMET_LIFTED = new ModelLayerLocation(MiningHelmetMod.locate("mining_helmet"), "lifted");
	/*public static final ModelLayerLocation LIGHT_BEAM = new ModelLayerLocation(MiningHelmetMod.locate("mining_helmet"), "light_beam");*/

	public static void init()
	{
		FMLJavaModLoadingContext.get().getModEventBus().addListener(ModModelLayers::initLayers);
	}

	public static void initPost(FMLClientSetupEvent event)
	{
	}

	public static void initLayers(EntityRenderersEvent.RegisterLayerDefinitions event)
	{
		event.registerLayerDefinition(MINING_HELMET, () -> MiningHelmetModel.createHelmetLayer(CubeDeformation.NONE, true));
		event.registerLayerDefinition(MINING_HELMET_LIFTED, () -> MiningHelmetModel.createHelmetLayer(CubeDeformation.NONE, false));

		/*event.registerLayerDefinition(LIGHT_BEAM, () -> LightBeamModel.createBeamLayer(CubeDeformation.NONE, true));*/
	}
}