package com.legacy.mining_helmet;

import org.apache.commons.lang3.tuple.Pair;

import net.minecraftforge.common.ForgeConfigSpec;

public class MiningHelmetConfig
{
	public static final ForgeConfigSpec CLIENT_SPEC;
	public static final ForgeConfigSpec SERVER_SPEC;
	public static final ClientConfig CLIENT;
	public static final ServerConfig SERVER;

	public static boolean helmetCoversFace()
	{
		return CLIENT.helmetCoversFace.get();
	}

	public static int lightRefreshRate()
	{
		return CLIENT.lightRefreshRate.get();
	}

	public static int maxVisibleDistance()
	{
		return CLIENT.maxVisibleDistance.get();
	}

	public static boolean seeThroughWalls()
	{
		return CLIENT.seeThroughWalls.get();
	}

	public static int getHelmetLightLevel()
	{
		return CLIENT.helmetLightLevel.get();
	}

	public static int minerSpawnChance()
	{
		return SERVER.minerSpawnChance.get();
	}

	static
	{
		{
			final Pair<ClientConfig, ForgeConfigSpec> clientPair = new ForgeConfigSpec.Builder().configure(ClientConfig::new);
			CLIENT = clientPair.getLeft();
			CLIENT_SPEC = clientPair.getRight();

			final Pair<ServerConfig, ForgeConfigSpec> serverPair = new ForgeConfigSpec.Builder().configure(ServerConfig::new);
			SERVER = serverPair.getLeft();
			SERVER_SPEC = serverPair.getRight();
		}
	}

	private static class ClientConfig
	{
		public final ForgeConfigSpec.ConfigValue<Boolean> helmetCoversFace;
		public final ForgeConfigSpec.ConfigValue<Integer> lightRefreshRate;
		public final ForgeConfigSpec.ConfigValue<Integer> maxVisibleDistance;
		public final ForgeConfigSpec.ConfigValue<Boolean> seeThroughWalls;
		public final ForgeConfigSpec.ConfigValue<Integer> helmetLightLevel;

		public ClientConfig(ForgeConfigSpec.Builder builder)
		{
			builder.comment("Miner's Helmet Client Config");
			
			builder.push("Helmet Visuals");
			helmetCoversFace = builder.comment("\n The Mining Helmet will cover the face of whatever mob is wearing it. Requires restarting the game.").define("helmetCoversFace", true);
			builder.pop();
			
			builder.push("Dynamic Lighting (NOTE: These options do nothing with Lucent installed)");
			lightRefreshRate = builder.comment("\n The rate at which the Mining Helmet refreshes it's light (measured in ticks). Lower values are smoother but result in worse fps.").defineInRange("lightRefreshRate", 2, 1, Integer.MAX_VALUE);
			maxVisibleDistance = builder.comment("\n How far away you should be able to see light from a mining helmet from other mobs (measured in blocks). 0 for infinite distance. Use this to prevent far away mobs from causing light updates.").defineInRange("maxVisibleDistance", 128, 0, Integer.MAX_VALUE);
			seeThroughWalls = builder.comment("\n Should dynamic lighting still occur if an entity is behind a wall. Turning this on may impact performance, but allows light to be seen around corners.").define("seeThroughWalls", false);
			helmetLightLevel = builder.comment("\n The light level emitted by the helmet.").defineInRange("helmetLightLevel", 12, 0, 15);
			builder.pop();
		}
	}

	private static class ServerConfig
	{
		public final ForgeConfigSpec.ConfigValue<Integer> minerSpawnChance;

		public ServerConfig(ForgeConfigSpec.Builder builder)
		{
			builder.comment("Miner's Helmet Common Config");
			
			builder.push("Mobs");
			minerSpawnChance = builder.comment("\n The chance for applicably tagged mobs to spawn with a Mining Helmet (1 in x, 0 prevents them from spawning with helmets)").define("minerSpawnChance", 20);
			builder.pop();
		}
	}
}
